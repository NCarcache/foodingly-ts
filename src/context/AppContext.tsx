import React, { useState, PropsWithChildren } from "react";
import useAxios from "axios-hooks";
import { ProductI } from "../utils/map.types";

interface Types{
  cartId: string;
  addProduct: (productId:string)=>Promise<void>;
  showCartProducts: ()=>Promise<ProductI[]>;
  removeProduct: (productId:string)=>Promise<void>;
  productCount: number;
}

const AppContext = React.createContext<Types | null>(null);

export const AppContextProvider = (props: PropsWithChildren<unknown>) => {
  const [cartId, setCartId] = useState<string>("");
  const [productCount, setProductCount] = useState<number>(0);
  const [,refetch] = useAxios("/cart", {
    manual: true,
  });

  const createCart = () =>
    refetch({
      method: "POST",
      data: {
        products: [],
      },
    });

  const addCartItem = (productId: string, cartId: string) =>
    refetch({
      method: "POST",
      url: "/cart/add-product",
      data: {
        cartId,
        productId,
      },
    });

  const addProduct = async (productId: string) => {
    if (!cartId) {
      const cart = await createCart();
      setCartId(cart.data.id);
      await addCartItem(productId, cart.data.id);
      setProductCount((prev) => prev + 1);
      return;
    }
    await addCartItem(productId, cartId);
    setProductCount((prev) => prev + 1);
    return;
  };

  const loadCart = (cartId: string) => {
    return refetch({
      method: "GET",
      url: `/cart/${cartId}`,
    });
  };

  const showCartProducts = async () => {
    const cartProducts = await loadCart(cartId);
    return cartProducts.data?.products;
  };

  const removeProduct = async (productId: string) => {
    await refetch({
      method: "POST",
      url: "/cart/remove-product",
      data: {
        cartId,
        productId,
      },
    });
    setProductCount((prev) => prev - 1);
    return;
  };

  return (
    <AppContext.Provider
      value={{
        cartId,
        addProduct,
        showCartProducts,
        removeProduct,
        productCount,
      }}
    >
      {props.children}
    </AppContext.Provider>
  );
};

export default AppContext;
