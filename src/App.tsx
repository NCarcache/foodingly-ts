import Axios from "axios";
import { configure } from "axios-hooks";
import LRU from "lru-cache";
import { AppContextProvider } from "./context/AppContext";
AppContextProvider;
import Home from "./components/Home";
import AppBar from "./components/AppBar";

const axios = Axios.create({
  baseURL: import.meta.env.VITE_API_URL,
  headers: {
    authorization: `Bearer ${import.meta.env.VITE_API_TOKEN}`,
  },
});

const cache = new LRU({ max: 10 });

configure({ axios, cache });

function App() {
  return (
    <AppContextProvider>
      <AppBar/>
      <Home />
    </AppContextProvider>
  );
}

export default App;
