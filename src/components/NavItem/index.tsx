import React from "react";
import Icon from "../Icon";
import Links from "../Link";

interface Props {
  label: string;
  options?: string[];
  className: string;
}

const NavItem = (props: Props) => {
  const { options, label, className } = props;

  const renderIcon = () => {
    if (!options?.length) return <></>;
    return <Icon className="fa-solid fa-angle-down" />;
  };

  const renderItems = () => {
    if (!options?.length) return <></>;
    return (
      <ul className="AppBarDropdown">
        {options?.map((option) => (
          <li className="AppBarDropdown-item" key={option}>
            <Links href="#" className="AppBarDropdown-link">
              {option}
            </Links>
          </li>
        ))}
      </ul>
    );
  };
  return (
    <li className={className}>
      {label} {renderIcon()}
      {renderItems()}
    </li>
  );
};

export default NavItem;
