import { ProductI } from "../../utils/map.types";
import Icon from "../Icon";
import Image from "../Image";

interface Props{
  product: ProductI;
  onDelete: (productId:string)=>void;
}

const Cart = (props:Props) => {
  const { product, onDelete } = props;
  return (
    <div className="CartProduct">
      <div className="CartProduct-info">
        <Image src={product.image} className="Cart-image" />
        <div className="Cart-content">
          <p className="Cart-name">{product.name.toLowerCase()}</p>
          <p className="Cart-price">
            <span className="Cart-span">1 x </span>${product.price}
          </p>
        </div>
      </div>
      <Icon
        className="fa-solid fa-trash-can Cart-delete"
        onClick={() => onDelete(product.id)}
      />
    </div>
  );
};

export default Cart;
