import { useContext, useEffect, useMemo, useState } from "react";
import { Dock } from "react-dock";
import AppContext from "../../context/AppContext";
import { ProductI } from "../../utils/map.types";
import Button from "../Button";
import Cart from "../Cart";
import Icon from "../Icon";

interface Props {
  isVisible: boolean;
  handleChange: ()=>void;
}

const CartModal = (props: Props):JSX.Element => {
  const { isVisible, handleChange } = props;
  const [products, setProducts] = useState<ProductI[]>([]);
  const context = useContext(AppContext);

  useEffect(() => {
    loadCart();
  }, [isVisible]);

  const loadCart = async () => {
    if (!isVisible) return;
    const list = await context?.showCartProducts?.();
    if(list) setProducts(list);
  };

  const onDelete = async (productId: string) => {
    await context?.removeProduct?.(productId);
    const list = await context?.showCartProducts?.();
    if(list)setProducts(list);
  };

  const renderCartProducts = () => {
    if (!products)
      return <div className="Cart-fallback">No products on your cart</div>;

    return products.map((product) => (
      <Cart product={product} onDelete={onDelete} key={product.id} />
    ));
  };

  const subTotal = useMemo(() => {
    if (!products) return;
    let sum = 0;
    products.forEach((product) => {
      sum = sum + product.price;
    });
    return sum;
  }, [products]);

  return (
    <Dock
      position="right"
      isVisible={isVisible}
      size={0.165}
      dimMode="opaque"
      onVisibleChange={handleChange}
    >
      <div className={`Cart ${isVisible ? "Cart-visible" : ""}`}>
        <div className="Cart-container">
          <div className="Cart-header">
            <p className="Cart-title">
              My Cart ({!products ? "0" : products.length})
            </p>
            <Icon className="fa-solid fa-xmark" onClick={handleChange} />
          </div>
          <div className="Cart-Body">{renderCartProducts()}</div>
          <div className="Cart-footer">
            <p className="Cart-subtotal">Subtotal</p>
            <span>${subTotal}</span>
          </div>
          <Button className="button slim">Checkout</Button>
        </div>
      </div>
    </Dock>
  );
};

export default CartModal;
