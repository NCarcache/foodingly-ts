import React from "react";

interface Props {
  src: string;
  alt?: string;
  className: string;
}

const Image = (props: Props): JSX.Element => {
  const { src, alt, className } = props;
  return <img src={src} alt={alt} className={className} />;
};

export default Image;
