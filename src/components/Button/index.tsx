import React, { PropsWithChildren } from "react";

interface Props {
  className: string;
  onClick?: () => void;
}

const Button = (props: PropsWithChildren<Props>):JSX.Element => {
  const { children, className, onClick } = props;
  return (
    <button className={className} onClick={onClick}>
      {children}
    </button>
  );
};

export default Button;
