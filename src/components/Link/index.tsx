import React, { PropsWithChildren } from 'react'

interface Props {
  className: string;
  href?: string;
}

const Link = (props: PropsWithChildren<Props>): JSX.Element => {
  const {className, href} = props;
  return (
    <a className={className} href={href}>{props.children}</a>
  )
}

export default Link;