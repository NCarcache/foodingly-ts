import React from "react";

interface Props {
  className: string;
  onClick?: () => void;
  dataContent?: number;
}

const Icon = (props: Props): JSX.Element => {
  const { className, onClick, dataContent } = props;
  return <i className={className} onClick={onClick} data-content={dataContent}/>;
};

export default Icon;
