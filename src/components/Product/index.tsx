import React, { useContext } from "react";
import AppContext from "../../context/AppContext";
import { ProductI } from "../../utils/map.types";
import Button from "../Button";
import Card from "../Card";
import CardBody from "../Card/CardBody";
import CardFooter from "../Card/CardFooter";
import CardHeader from "../Card/CardHeader";
import Image from "../Image";
import Link from "../Link";

interface Props{
  product: ProductI;
}

const Product = (props: Props) => {

  const {product} = props;

  const context = useContext(AppContext);

  const onAdd = () =>{
    return context?.addProduct(product.id);
  }

  const renderDiscount = () =>{
    if (product.discount){
      return <div className="ProductCard-discount">{product.discount}%</div>
    }
  }
  return (
    <Card className="ProductCard" key={product.id}>
      <CardHeader className="ProductCard-header">
        <Image className="ProductCard-image" src={product.image} />
        {renderDiscount()}
      </CardHeader>
      <CardBody className="ProductCard-content">
        <h3 className="ProductCard-title">
          <Link className="ProductCard-link" href="#">
            {product.name.toLowerCase()}
          </Link>
        </h3>
        <p className="ProductCard-text">
          <span className="ProductCard-rating">4.8/5 Excellent</span>
          <span className="ProductCard-review">(1214 reviews)</span>
        </p>
      </CardBody>
      <CardFooter className="ProductCard-footer">
        <p className="ProductCard-price">${product.price}</p>
        <Button className="ProductCard-button" onClick={onAdd}>Add to Cart</Button>
      </CardFooter>
    </Card>
  );
};

export default Product;
