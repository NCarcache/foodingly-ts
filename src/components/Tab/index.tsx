import React, { PropsWithChildren } from "react";

interface Props {
  className: string;
  onClick: () => void;
}

const Tab = (props: PropsWithChildren<Props>) => {

  const { className, onClick, children } = props;
  return (
    <button className={className} onClick={onClick}>
      {children}
    </button>
  );
};

export default Tab;
