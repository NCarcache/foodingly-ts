import { useContext, useState } from "react";
import Icon from "../Icon";
import Image from "../Image";
import Links from "../Link";
import NavItem from "../NavItem";
import CartModal from "../Cart/CartModal";
import AppContext from "../../context/AppContext";

const foodMenu = [
  "Food Grid",
  "Food Details",
  "Cart View",
  "Checkou View",
  "Order Success",
];
const blog = ["Blog", "Blog Details"];
const pages = [
  "About us",
  "Resevation",
  "Chefs",
  "Testimonials",
  "FAQ",
  "User Pages",
  "Customer Dashboard",
  "Privacy Policy",
  "Terms of Service",
  "404 Error",
];

const AppBar = () => {
  const [isVisible, setisVisible] = useState<boolean>(false);

  const context = useContext(AppContext);

  const handleChange = () => {
    setisVisible(!isVisible);
  };

  return (
    <nav className="AppBar">
      <div className="AppBar-container">
        <div className="AppBar-options">
            <Image
              src="https://foodingly.netlify.app/assets/img/logo.png"
              alt="Foodingly Logo"
              className="AppBar-logo"
            />
          <ul className="AppBar-menu">
            <li className="AppBar-item">
              <Links className="AppBar-link">
                Home
              </Links>
            </li>
            <NavItem label="About Us" className="AppBar-item" />
            <NavItem
              label="Food Menu"
              className="AppBar-item FoodMenu"
              options={foodMenu}
            />
            <NavItem
              label="Blog"
              className="AppBar-item BlogD"
              options={blog}
            />
            <NavItem
              label="Pages"
              className="AppBar-item Page"
              options={pages}
            />
            <li className="AppBar-item">
              <Links className="AppBar-link">
                Contact
              </Links>
            </li>
          </ul>
        </div>

        <div className="AppBar-buttons">
          <div className="AppBar-shopping">
            <Icon
              className="fa-solid fa-bag-shopping AppBar-icon notification"
              onClick={handleChange}
              dataContent={context?.productCount}
            />
          </div>
          <Icon className="fa-solid fa-magnifying-glass AppBar-icon green" />
          <Links className="link Secondary">Reservation</Links>
        </div>
      </div>
      <CartModal isVisible={isVisible} handleChange={handleChange} />
    </nav>
  );
};

export default AppBar;
