import Products from './Products'

const index = () => {
  return (
    <main>
      <Products/>
    </main>
  )
}

export default index