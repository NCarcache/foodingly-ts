import React, { useEffect, useState } from "react";
import useAxios from "axios-hooks";
import Section from "../Section";
import Product from "../Product";
import Tab from "../Tab";
import { ProductI } from "../../utils/map.types";

enum Catgeory {
  NOODLES = "NOODLES",
  BURGER = "BURGER",
  CHICKEN = "CHICKEN",
  ICE_CREAM = "ICE_CREAM",
  DRINKS = "DRINKS"
}

const Products = () => {
  const [{ data }, refetch] = useAxios("/products");
  const [list, setList] = useState<ProductI[]>([]);
  const [isActive, setIsActive] = useState<string>("");

  useEffect(() => {
    if (!data) return;
    setList(data);
  }, [data]);

  const filterList = async (category: string) => {
    await refetch({
      params: {
        category,
      },
    });
    setIsActive(category);
  };

  const renderProducts = () =>
    list.map((product) => <Product product={product} key={product.id} />);

  return (
    <Section
      heading="Our Fetured Items"
      title="Our most popular items"
      className="Product-container"
    >
      <div className="Product-categories">
        <Tab
          className={`Product-tab ${isActive === "" ? "Product-active" : ""}`}
          onClick={() => filterList("")}
        >
          All Categories
        </Tab>
        <Tab
          className={`Product-tab ${
            isActive === Catgeory.NOODLES ? "Product-active" : ""
          }`}
          onClick={() => filterList(Catgeory.NOODLES)}
        >
          Noodles
        </Tab>
        <Tab
          className={`Product-tab ${
            isActive === Catgeory.BURGER ? "Product-active" : ""
          }`}
          onClick={() => filterList(Catgeory.BURGER)}
        >
          Burger
        </Tab>
        <Tab
          className={`Product-tab ${
            isActive === Catgeory.CHICKEN ? "Product-active" : ""
          }`}
          onClick={() => filterList(Catgeory.CHICKEN)}
        >
          Chicken
        </Tab>
        <Tab
          className={`Product-tab ${
            isActive === Catgeory.ICE_CREAM ? "Product-active" : ""
          }`}
          onClick={() => filterList(Catgeory.ICE_CREAM)}
        >
          Ice Cream
        </Tab>
        <Tab
          className={`Product-tab ${
            isActive === Catgeory.DRINKS ? "Product-active" : ""
          }`}
          onClick={() => filterList(Catgeory.DRINKS)}
        >
          Drinks
        </Tab>
      </div>
      <div className="Product">{renderProducts()}</div>
    </Section>
  );
};

export default Products;
