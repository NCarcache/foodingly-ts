import React, {PropsWithChildren} from "react";

interface Props {
  className: string;
  title: string;
  heading: string;
}

const Section = (props: PropsWithChildren<Props>): JSX.Element => {

  const {heading, title, className, children} = props;

  return (
    <section className="Section">
      <p className="Section-heading">{heading}</p>
      <h3 className="Section-title"><span>{title}</span></h3>
      <div className={className}>{children}</div>
    </section>
  );
};

export default Section;
