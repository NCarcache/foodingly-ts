import React, { PropsWithChildren } from "react";

interface Props {
  className: string;
}

const CardFooter = (props: PropsWithChildren<Props>):JSX.Element => {
  const { className, children } = props;

  return <div className={className}>{children}</div>;
};

export default CardFooter;
