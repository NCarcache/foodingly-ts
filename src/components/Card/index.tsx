import React, { PropsWithChildren } from "react";
interface Props {
  className: string;
}

const Card = (props: PropsWithChildren<Props>):JSX.Element => {
  const { className, children } = props;

  return <div className={`Card ${className}`}>{children}</div>;
};

export default Card;
