import React, { PropsWithChildren } from "react";

interface Props{
  className:string;
  style?:React.CSSProperties;
}

const CardHeader = (props: PropsWithChildren<Props>): JSX.Element => {
  const { className, children, style } = props;
  return <div className={className} style={style}>{children}</div>;
};

export default CardHeader;
