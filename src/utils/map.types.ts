export interface ProductI{
    id: string;
    createdAt: string;
    updatedAt: string;
    deletedAt: string;
    name: string;
    price: number;
    image: string;
    category: string;
    discount: number;
}